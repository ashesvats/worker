module.exports = {
  /**
   * Application configuratio section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
   */
  apps : [

    // First application
    {
      name      : "SMS",
      script    : "smsworker.js",
      instances : 1,
      exec_mode : 'cluster',
      watch:true,
      env: {
        COMMON_VARIABLE: "true",
        TZ:"Asia/Kolkata"
      },
      env_production : {
        NODE_ENV: "production",
        DB_URL:"mongodb://openusers:HipcaskUsers2016@ds163917-a0.mlab.com:63917,ds163917-a1.mlab.com:63917/hipcaskapi?replicaSet=rs-ds163917",
        PORT:8080
      },
      env_dev:{

      }
    },{
      name      : "Email",
      script    : "emailworker.js",
      instances : 1,
      exec_mode : 'cluster',
      watch:true,
      env: {
        COMMON_VARIABLE: "true",
        TZ:"Asia/Kolkata"
      },
      env_production : {
        NODE_ENV: "production",
        DB_URL:"mongodb://openusers:HipcaskUsers2016@ds163917-a0.mlab.com:63917,ds163917-a1.mlab.com:63917/hipcaskapi?replicaSet=rs-ds163917",
        PORT:8080
      },
      env_dev:{

      }
    }
  ],

  /**
   * Deployment section
   * http://pm2.keymetrics.io/docs/usage/deployment/
   */
  deploy : {
    production : {
      user : "ubuntu",
      host : "35.154.65.152",
      key: "/home/yeoman/Downloads/HipApi.pem",
      ref  : "origin/master",
      repo : "https://ashesvats@bitbucket.org/ashesvats/worker.git",
      path : "/var/www/production",
      "post-deploy" : "npm install && pm2 startOrRestart ecosystem.config.js --env production 0"
    },
    dev : {
      user : "node",
      host : "212.83.163.1",
      ref  : "origin/master",
      repo : "git@github.com:repo.git",
      path : "/var/www/development",
      "post-deploy" : "npm install && pm2 startOrRestart ecosystem.config.js --env dev 0",
      env  : {
        NODE_ENV: "dev"
      }
    }
  }
}
